import React from 'react';
import logo from './logo.svg';
import './App.css';

const Test = ({title}) => {
  return (
    <h1>{title}</h1>
  )
};

function App() {
  return (
    <div className="App">
      <Test title="titre test"></Test>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
